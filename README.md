# McftEmbargo

Set an embargo on certain items so that they cannot be transferred between worlds. Block groups of items, such as enchanted items, armour, and weapons.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- Issues --  Builds -- Download -- Bukkit Dev