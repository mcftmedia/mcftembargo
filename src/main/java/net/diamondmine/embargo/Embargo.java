package net.diamondmine.embargo;

import java.io.File;

/**
 * Main framework interface.
 * 
 * @author Jon la Cour
 * @see EmbargoInstance
 */
public interface Embargo {

    /**
     * Get the directory used to store configuration.
     * 
     * @return directory used to store configuration
     */
    File getDataDirectory();

}
