package net.diamondmine.embargo.config.types;

import net.diamondmine.embargo.config.Builder;
import net.diamondmine.embargo.config.ConfigurationNode;
import net.diamondmine.embargo.config.Loader;
import net.diamondmine.embargo.util.MapBuilder.ObjectMapBuilder;

import com.sk89q.worldedit.BlockVector2D;

public class BlockVector2dLoaderBuilder implements Loader<BlockVector2D>, Builder<BlockVector2D> {

    public Object write(BlockVector2D value) {
        return new ObjectMapBuilder().put("x", value.getBlockX()).put("z", value.getBlockZ()).map();
    }

    public BlockVector2D read(Object value) {
        ConfigurationNode node = new ConfigurationNode(value);
        Double x = node.getDouble("x");
        Double z = node.getDouble("z");

        if (x == null || z == null) {
            return null;
        }

        return new BlockVector2D(x, z);
    }

}
