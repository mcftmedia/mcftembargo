package net.diamondmine.embargo.config.types;

import net.diamondmine.embargo.config.Builder;
import net.diamondmine.embargo.config.Loader;

public class StringLoaderBuilder implements Loader<String>, Builder<String> {

    public Object write(String value) {
        return String.valueOf(value);
    }

    public String read(Object value) {
        return String.valueOf(value);
    }

}
