package net.diamondmine.embargo.config.types;

import java.util.Map;

import net.diamondmine.embargo.config.Builder;
import net.diamondmine.embargo.config.Loader;

class MapLoaderBuilder implements Loader<Map<Object, Object>>, Builder<Map<Object, Object>> {

    public Object write(Map<Object, Object> value) {
        return value;
    }

    @SuppressWarnings("unchecked")
    public Map<Object, Object> read(Object value) {
        if (value instanceof Map) {
            return (Map<Object, Object>) value;
        } else {
            return null;
        }
    }

}
