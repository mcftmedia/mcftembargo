package net.diamondmine.embargo.config.types;

import java.util.Map;

import net.diamondmine.embargo.config.Builder;
import net.diamondmine.embargo.config.ConfigurationNode;
import net.diamondmine.embargo.config.Loader;

public class NodeLoaderBuilder implements Loader<ConfigurationNode>, Builder<ConfigurationNode> {

    public Object write(ConfigurationNode value) {
        return value;
    }

    public ConfigurationNode read(Object value) {
        if (value instanceof Map) {
            return new ConfigurationNode(value);
        } else {
            return null;
        }
    }

}
