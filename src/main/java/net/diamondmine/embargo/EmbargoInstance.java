package net.diamondmine.embargo;

/**
 * Holds a global instance of {@link Embargo}.
 * 
 * @author Jon la Cour
 */
public class EmbargoInstance {

    private static Embargo embargo;

    private EmbargoInstance() {
    }

    /**
     * Get an instance of McftEmbargo.
     * 
     * @return an instance of McftEmbargo
     */
    public static Embargo getInstance() {
        if (embargo == null) {
            throw new RuntimeException("Uh oh, McftEmbargo is not loaded as a plugin, but the API is being used!");
        }

        return embargo;
    }

    /**
     * Get whether an instance is available.
     * 
     * @return true if there's an instance
     */
    public static boolean hasInstance() {
        return embargo != null;
    }

    /**
     * Set the instance of McftEmbargo. This should only be called by
     * McftEmbargo itself.
     * 
     * @param embargo the instance of McftEmbargo
     */
    public static void setInstance(Embargo embargo) {
        EmbargoInstance.embargo = embargo;
    }

}
