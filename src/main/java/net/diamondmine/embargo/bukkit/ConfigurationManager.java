package net.diamondmine.embargo.bukkit;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.bukkit.World;

import com.sk89q.util.yaml.YAMLFormat;
import com.sk89q.util.yaml.YAMLProcessor;

/**
 * Configuration
 * 
 * @author Jon la Cour
 */
public class ConfigurationManager {

    private static final String CONFIG_HEADER = "#\r\n" + "# McftEmbargo's main configuration file\r\n" + "#\r\n"
            + "# About editing this file:\r\n"
            + "# - DO NOT USE TABS. You MUST use spaces or Bukkit will complain. If\r\n"
            + "#   you use an editor like Notepad++ (recommended for Windows users), you\r\n"
            + "#   must configure it to \"replace tabs with spaces.\" In Notepad++, this can\r\n"
            + "#   be changed in Settings > Preferences > Language Menu.\r\n" + "# - If you want to check the format of this file before putting it\r\n"
            + "#   into McftEmbargo, paste it into http://yaml-online-parser.appspot.com/\r\n" + "#   and see if it gives \"ERROR:\".\r\n"
            + "# - Lines starting with # are comments and so they are ignored.\r\n" + "#\r\n";

    /**
     * Reference to the plugin.
     */
    private EmbargoPlugin plugin;
    
    /**
     * Holds configurations for different worlds.
     */
    private ConcurrentMap<String, WorldConfiguration> worlds;

    /**
     * The global configuration
     */
    private YAMLProcessor config;

    /**
     * Construct the object.
     * 
     * @param plugin The plugin instance
     */
    public ConfigurationManager(EmbargoPlugin plugin) {
        this.plugin = plugin;
        this.worlds = new ConcurrentHashMap<String, WorldConfiguration>();
    }

    /**
     * Load the configuration.
     */
    public void load() {
        // Create the default configuration file
        plugin.createDefaultConfiguration(new File(plugin.getDataFolder(), "config.yml"), "config.yml");

        config = new YAMLProcessor(new File(plugin.getDataFolder(), "config.yml"), true, YAMLFormat.EXTENDED);
        try {
            config.load();
        } catch (IOException e) {
            plugin.getLogger().severe("Error reading configuration for global config: ");
            e.printStackTrace();
        }

        // Load configurations for each world
        for (World world : plugin.getServer().getWorlds()) {
            get(world);
        }

        config.setHeader(CONFIG_HEADER);

        if (!config.save()) {
            plugin.getLogger().severe("Error saving configuration!");
        }
    }
    
    /**
     * Unload the configuration.
     */
    public void unload() {
        worlds.clear();
    }
    
    /**
     * Get the configuration for a world.
     *
     * @param world The world to get the configuration for
     * @return {@code world}'s configuration
     */
    public WorldConfiguration get(World world) {
        String worldName = world.getName();
        WorldConfiguration config = worlds.get(worldName);
        WorldConfiguration newConfig = null;

        while (config == null) {
            if (newConfig == null) {
                newConfig = new WorldConfiguration(plugin, worldName, this.config);
            }
            worlds.putIfAbsent(world.getName(), newConfig);
            config = worlds.get(world.getName());
        }

        return config;
    }
}