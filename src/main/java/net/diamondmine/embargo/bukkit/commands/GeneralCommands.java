package net.diamondmine.embargo.bukkit.commands;

import net.diamondmine.embargo.bukkit.EmbargoPlugin;

import org.bukkit.command.CommandSender;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.NestedCommand;

public class GeneralCommands {
    @SuppressWarnings("unused")
    private final EmbargoPlugin plugin;

    public GeneralCommands(EmbargoPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = {"embargo"}, desc = "Embargo commands")
    @NestedCommand({EmbargoCommands.class})
    public void embargo(CommandContext args, CommandSender sender) {
    }

}
