package net.diamondmine.embargo.bukkit.commands;

import java.io.IOException;

import net.diamondmine.embargo.bukkit.EmbargoPlugin;
import net.diamondmine.embargo.config.ConfigurationException;

import org.bukkit.command.CommandSender;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;

public class EmbargoCommands {
    @SuppressWarnings("unused")
    private final EmbargoPlugin plugin;

    public EmbargoCommands(EmbargoPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(aliases = {"add", "ban"}, desc = "Adds an item to the embargo list.")
    public void add(CommandContext args, CommandSender sender) throws CommandException, IOException, ConfigurationException {
        
    }

}
