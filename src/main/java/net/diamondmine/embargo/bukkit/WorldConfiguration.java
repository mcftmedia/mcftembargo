package net.diamondmine.embargo.bukkit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;

import com.sk89q.util.yaml.YAMLFormat;
import com.sk89q.util.yaml.YAMLProcessor;

/**
 * Holds the configuration for individual worlds.
 * 
 * @author Jon la Cour
 */
public class WorldConfiguration {

    public static final String CONFIG_HEADER = "#\r\n" + "# McftEmbargo's world configuration file\r\n" + "#\r\n"
            + "# Items listed in this embargo will be banned from being brought into this world.\r\n"
            + "# Use item ID's only.\r\n" + "#\r\n";

    private EmbargoPlugin plugin;

    private String worldName;
    private YAMLProcessor parentConfig;
    private YAMLProcessor config;
    
    /* Configuration data start */
    public Set<String> blockedItems;

    /**
     * Construct the object.
     * 
     * @param plugin The WorldGuardPlugin instance
     * @param worldName The world name that this WorldConfiguration is for.
     * @param parentConfig The parent configuration to read defaults from
     */
    public WorldConfiguration(EmbargoPlugin plugin, String worldName, YAMLProcessor parentConfig) {
        File baseFolder = new File(plugin.getDataFolder(), "worlds/" + worldName);
        File configFile = new File(baseFolder, "config.yml");

        this.plugin = plugin;
        this.worldName = worldName;
        this.parentConfig = parentConfig;

        plugin.createDefaultConfiguration(configFile, "config_world.yml");

        config = new YAMLProcessor(configFile, true, YAMLFormat.EXTENDED);
        loadConfiguration();
    }

    /**
     * Load the configuration.
     */
    private void loadConfiguration() {
        try {
            config.load();
        } catch (IOException e) {
            plugin.getLogger().severe("Error reading configuration for world " + worldName + ": ");
            e.printStackTrace();
        }

        for (String item : getStringList("embargo.items", null)) {
            Material material = getMaterial(item);
            
            if (material != null) {
                blockedItems.add(material.toString());
            }
        }

        config.setHeader(CONFIG_HEADER);

        config.save();
    }

    private List<String> getStringList(String node, List<String> def) {
        List<String> res = parentConfig.getStringList(node, def);

        if (res == null || res.size() == 0) {
            parentConfig.setProperty(node, new ArrayList<String>());
        }

        if (config.getProperty(node) != null) {
            res = config.getStringList(node, def);
        }

        return res;
    }
    
    private Material getMaterial(String item) {
        Integer id = null;
        Material material = null;
        
        try {
            id = Integer.parseInt(item);
        } catch (NumberFormatException e) {
            plugin.getLogger().warning("Invalid integer '" + item + "'");
        }
        
        try {
            if (id != null) {
                material = Material.getMaterial(id);
            }
        } catch (Exception e) {
            plugin.getLogger().warning("Unknown item ID '" + item + "'");
        }
        
        return material;
    }

    public String getWorldName() {
        return this.worldName;
    }

}
